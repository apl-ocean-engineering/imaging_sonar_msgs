# The imaging_sonar_msgs pkg

**Note:** The message types in this repository are functionally obsolete.  
The relevant message types are now in the [acoustic_msgs](https://github.com/apl-ocean-engineering/acoustic_msgs) repository, though
these messages may still be required for backwards compatibility with bags
containing messages from the `imaging_sonar_msgs/` namespace.

The abstract `AbstractSonarInterface` type is still relevant.

Defines ROS messages for 2D multibeam "imaging" sonars.

# Licensing

This repository is covered by the [BSD License](LICENSE.txt).
